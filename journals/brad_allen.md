Everything before this was mostly design, etc.
May 11 - First pass of adding map to 'main'
May 12 - Merged my branch onto 'main'
May 15 - Started working on markers for the map.
May 22 - Getting map working 90% (Learned a lot about Google maps API, etc.)
May 23 - Still working on map.
May 25 - Worked on backend.
May 26 - Worked on backend.
May 27 - Worked on backend.
May 29 - Worked on backend.
May 30 - Reimplemented JWTdown into main.
May 31 - Finished JWTdown and Axios
Jun 1 - Little stuff.
Jun 2 - Fixed user update, fixed markers, worked on maps.
Jun 6 - Fixed feeds router, finished implementing AWS. (Learned a lot about how AWS works, presigned URLs, etc. Not too bad actually.)
Jun 7 - Lots of Cirrus fighting.
Jun 8 - Completed deployment, worked on CSS.
Jun 9 0 Worked on CSS/Posts
<3
