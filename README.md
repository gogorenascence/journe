JournE

Fermin Santiago
Jin Park
Brad Allen
Myla Smith

JournE – The exploration of new places and the discovery of hidden gems that may be off the beaten path
JournE - inspiring others to embark on their own journeys of exploration
JournE - share real and unfiltered moments that capture the true essence of your city


Design

FastAPI
GHI
PostgreSQL
Integrations - GoogleMaps API
AWS


Intended market

The intended market for JournE would be both tourists and local individuals seeking engaging experiences and activities in their area. Tourists visiting a new city or town often desire guidance on where to go and what to do, making JournE a valuable resource for them. By using the interactive map feature and browsing user-generated posts with photos, they can discover popular hotspots and hidden gems recommended by locals. Additionally, JournE caters to residents who want to explore their own neighborhoods, discover new places, and share their favorite spots with others. The website provides a platform for the community to connect, exchange recommendations, and foster a sense of belonging by collectively showcasing the best aspects of their city or region.


Functionality

Users can create and account and login in, once doing so, they can use our map (which automatically grabs our users location) and click on their intended hotspot to create a post.

Users can view posts made by other visitors

If logged in, users can view the list of accounts on our platform

User can visit their specific profile to make updates to their account and also view their posts

User can update and delete their existing posts

About Us page features information about our company with photos of the founders and links to their linked in pages

User can upload photo directly from their local computer when creating a post through AWS


Project Initialization

Click this link: https://jinp36.gitlab.io/module3-project-gamma

OR

Follow these steps:

Clone the repository down to your local machine

CD into the new project directory

Run docker volume create sample_service

Run docker compose build

Run docker compose up
