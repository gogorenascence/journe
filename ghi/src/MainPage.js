

import MapPage from "./MapPage"

function MainPage() {

  return (
    <>
      <header>
      </header>

      <MapPage />

      <footer>
        <div>

          <div class="row">
            JournE Copyright © 2023 - All rights reserved
          </div>
        </div>
      </footer>
    </>
  );
}

export default MainPage;
