import React, { useState, useEffect } from "react";
import axios from "axios";

const token = localStorage.getItem("token");


export default function UserList() {
  const [users, setUsers] = useState([]);
  const [errorMessage, setErrorMessage] = useState('')

  const getUsers = async () => {
    axios.get(`${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/users`, {
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })
      .then(response => {

        const data = response
        setUsers(data.data.users)
      }
      )
      .catch(err => { if (401 === err.response.status) { setErrorMessage("Not authorized to view!"); console.log("error!") }; })
  }

  useEffect(() => {
    getUsers();
  }, []);

  const deleteUser = async (id) => {
    const url = `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/users/${id}/`;
    try {
      const response = await fetch(url, {
        method: "DELETE",
      });
      if (response.ok) {
        setUsers(users.filter((user) => user.id !== id));
      }
    } catch (error) {
      console.error("Could not delete user:", error);
    }
  };


  return (
    <div id="form-row" className="row">
      <div className="container-responsive shadow p-4" style={{ backgroundColor: "rgba(255, 255, 255, 0.89)", borderRadius: "10px" }}>
        <h1 className="text-center">List of Accounts</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Username</th>
              <th>Email</th>
              <th>Password</th>
              <th>Zodiac Sign</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {users.map((user) => (
              <tr key={user.id}>
                <td>
                  {user.first} {user.last}
                </td>
                <td>{user.username}</td>
                <td>{user.email}</td>
                <td>{user.password}</td>
                <td>{user.zodiac_sign}</td>
                <td>
                  <button
                    className="btn btn-outline-danger fw-bold"
                    onClick={() => deleteUser(user.id)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        {errorMessage && (<p style={{ color: "red" }}>{errorMessage}</p>)}
      </div>

    </div>
  );
}
