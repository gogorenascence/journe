import {
  React,
  useState,
  useCallback,
  useEffect,
  // useRef,
} from "react";

import { useNavigate } from "react-router-dom";
import axios from 'axios';


import "./MapStyles.css";

import {
  GoogleMap,
  useJsApiLoader,
  MarkerF,
  InfoWindowF,
} from "@react-google-maps/api";

const token = localStorage.getItem("token");

function MyComponent() {
  const navigate = useNavigate();
  const { isLoaded } = useJsApiLoader({
    id: "google-map-script",
    googleMapsApiKey: "AIzaSyAUXR389LzF_ - iI66tbAmVmP_bstAOGQtU",
  });

  const [lat, setLat] = useState(null);
  const [long, setLong] = useState(null);
  const [map, setMap] = useState(null);
  const [southWest, setSouthWest] = useState(null);
  const [northEast, setNorthEast] = useState(null);
  const [mapData, setMapData] = useState([]);


  const [mapInstance, setMapInstance] = useState(null);
  const [tempMarkerPosition, setTempMarkerPosition] = useState(null);
  const [showInfoWindow, setShowInfoWindow] = useState(false);
  const [showOtherInfoWindows, setShowOtherInfoWindows] = useState(null);
  const [contentReady, setContentReady] = useState(false);
  const [apiMarkers, setApiMarkers] = useState([]);
  const [markersWithinBounds, setMarkersWithinBounds] = useState([]);

  const geolocationAPI = navigator.geolocation;

  const createMarkersWithinBounds = (data, bounds) => {
    const markersWithinBounds = [];
    const existingMarkerIds = [];

    for (const item of data) {
      const { id, user_id, subject, favorite, longitude, latitude, picture_url } = item;

      if (existingMarkerIds.includes(id)) {
        continue;
      }

      const marker = {
        id,
        user_id,
        subject,
        favorite,
        position: {
          lat: latitude,
          lng: longitude,
        },
        picture_url
      };

      markersWithinBounds.push(marker);
      existingMarkerIds.push(id);
    }

    return markersWithinBounds;
  };


  const fitMapToBounds = (map, bounds) => {
    map.fitBounds(bounds);
    map.setZoom(14);
  };

  const setLocation = () => {
    geolocationAPI.getCurrentPosition(
      (position) => {
        const { coords } = position;
        setLat(coords.latitude);
        setLong(coords.longitude);
      },
      (error) => {
        setLat(38.87899512619728);
        setLong(-85.69443622777568);
      }
    );
  };

  useEffect(() => {
    if (!geolocationAPI) {
    } else {
      setLocation();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [geolocationAPI]);


  const handleMapClick = (event) => {
    if (showInfoWindow === false && showOtherInfoWindows === null) {
      const clickedPosition = {
        lat: event.latLng.lat(),
        lng: event.latLng.lng(),
      };
      setTimeout(() => {
        setShowOtherInfoWindows(null);
        setTempMarkerPosition(clickedPosition);
        setShowInfoWindow(true);
      }, 100);
      setTimeout(() => {
        setContentReady(true);
      }, 250);
    } else if (showOtherInfoWindows != null) {
      setShowOtherInfoWindows(null);
      setTimeout(() => {
        const clickedPosition = {
          lat: event.latLng.lat(),
          lng: event.latLng.lng(),
        };
        setTempMarkerPosition(clickedPosition);
        setShowInfoWindow(true);
      }, 150);
      setTimeout(() => {
        setContentReady(true);
      }, 250);
    } else {
      setShowInfoWindow(false);
      setTempMarkerPosition(null);
      setContentReady(false);
    }
  };


  const getLocations = async () => {

    if (southWest != null) {
      axios.get(`${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/locations`, {
        params: {
          from_lat: southWest.lat() - .7,
          to_lat: northEast.lat() + .7,
          from_lng: southWest.lng() - .7,
          to_lng: northEast.lng() + .7,
        },
        headers: {
          'Authorization': `Bearer ${token}`
        }
      })
        .then(response => {
          const newMarkers = response.data.feeds;

          newMarkers.forEach(newMarker => {
            const markerExists = apiMarkers.some(marker => marker.id === newMarker.id);
            if (!markerExists) {
              setApiMarkers(prevMarkers => [...prevMarkers, newMarker]);
            }

          })

        }
        )
    }
  };

  const initializeMap = (map) => {
    if (window.google && window.google.maps) {
      const bounds = new window.google.maps.LatLngBounds();
      setLocation();
      if (lat && long) {
        const userPosition = { lat, lng: long };
        bounds.extend(userPosition);
      }
      setTimeout(() => {
        fitMapToBounds(map, bounds);
      }, 100);

      setSouthWest(bounds.getSouthWest());
      setNorthEast(bounds.getNorthEast());
      getLocations();

      setMapInstance(map);

      setTimeout(() => {
        createMarkersWithinBounds(apiMarkers, bounds);
        refreshMap();
      }, 200);
    }
  };

  useEffect(() => {
    if (lat !== null && long !== null) {
      initializeMap(mapInstance);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [lat, long, mapInstance]);

  const handleCreateMarkers = () => {
    const bounds = mapInstance.getBounds();

    const markersWithinBounds = createMarkersWithinBounds(apiMarkers, bounds);
    setMarkersWithinBounds(markersWithinBounds)
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const onUnmount = useCallback(function callback(map) {
    setMap(null);
    setMarkersWithinBounds([]);
    setApiMarkers([]);

  });

  const onMarkerClick = (marker) => {
    if (showInfoWindow === true) {
      setShowInfoWindow(false);
      setContentReady(false);
      setTempMarkerPosition(null);
    }

    setTimeout(() => {
      setShowOtherInfoWindows(marker.id);
    }, 150);
  };

  const buttonClick = (southWest, northEast) => {

    const params = {
      northEast,
      southWest
    };
    setTimeout(() => {

      navigate(`/feeds/new`, { state: params });
    }, 900);
  };


  const handleMapBoundsChange = () => {
    if (mapInstance) {
      const bounds = mapInstance.getBounds();
      if (bounds) {
        const southWest = bounds.getSouthWest();
        const northEast = bounds.getNorthEast();

        setSouthWest(southWest);
        setNorthEast(northEast);

        handleCreateMarkers();
      }
    }
  };

  const refreshMap = () => {
    setMapData({});
    console.log(mapData)
  };

  const options = {
    clickableIcons: false,
    styles: [
      {
        featureType: "poi.business",
        elementType: "labels.icon",
        stylers: [{ visibility: "off" }],
      },
      {
        featureType: "poi.park",
        elementType: "labels.icon",
        stylers: [{ visibility: "off" }],
      },
      {
        featureType: "poi.school",
        elementType: "labels.icon",
        stylers: [{ visibility: "off" }],
      },
      {
        featureType: "transit",
        elementType: "labels.icon",
        stylers: [{ visibility: "off" }],
      },
      {
        featureType: "water",
        elementType: "geometry",
        stylers: [
          {
            hue: "#165c64",
          },
          {
            saturation: 34,
          },
          {
            lightness: -69,
          },
          {
            visibility: "on",
          },
        ],
      },
      {
        featureType: "landscape",
        elementType: "geometry",
        stylers: [
          {
            hue: "#b7caaa",
          },
          {
            saturation: -14,
          },
          {
            lightness: -18,
          },
          {
            visibility: "on",
          },
        ],
      },
      {
        featureType: "landscape.man_made",
        elementType: "all",
        stylers: [
          {
            hue: "#cbdac1",
          },
          {
            saturation: -6,
          },
          {
            lightness: -9,
          },
          {
            visibility: "on",
          },
        ],
      },
      {
        featureType: "road",
        elementType: "geometry",
        stylers: [
          {
            hue: "#8d9b83",
          },
          {
            saturation: -89,
          },
          {
            lightness: -12,
          },
          {
            visibility: "on",
          },
        ],
      },
      {
        featureType: "road.highway",
        elementType: "geometry",
        stylers: [
          {
            hue: "#d4dad0",
          },
          {
            saturation: -88,
          },
          {
            lightness: 54,
          },
          {
            visibility: "simplified",
          },
        ],
      },
      {
        featureType: "road.arterial",
        elementType: "geometry",
        stylers: [
          {
            hue: "#bdc5b6",
          },
          {
            saturation: -89,
          },
          {
            lightness: -3,
          },
          {
            visibility: "simplified",
          },
        ],
      },
      {
        featureType: "road.local",
        elementType: "geometry",
        stylers: [
          {
            hue: "#bdc5b6",
          },
          {
            saturation: -89,
          },
          {
            lightness: -26,
          },
          {
            visibility: "on",
          },
        ],
      },
      {
        featureType: "poi",
        elementType: "geometry",
        stylers: [
          {
            hue: "#c17118",
          },
          {
            saturation: 61,
          },
          {
            lightness: -45,
          },
          {
            visibility: "on",
          },
        ],
      },
      {
        featureType: "poi.park",
        elementType: "all",
        stylers: [
          {
            hue: "#8ba975",
          },
          {
            saturation: -46,
          },
          {
            lightness: -28,
          },
          {
            visibility: "on",
          },
        ],
      },
      {
        featureType: "transit",
        elementType: "geometry",
        stylers: [
          {
            hue: "#a43218",
          },
          {
            saturation: 74,
          },
          {
            lightness: -51,
          },
          {
            visibility: "simplified",
          },
        ],
      },

      {
        featureType: "administrative",
        elementType: "labels.text.fill",
        stylers: [
          {
            color: "#6195a0",
          },
          {
            weight: "20",
          },
        ],
      },

      {
        featureType: "poi.medical",
        elementType: "geometry",
        stylers: [
          {
            hue: "#cba923",
          },
          {
            saturation: 50,
          },
          {
            lightness: -46,
          },
          {
            visibility: "on",
          },
        ],
      },
    ],
  };

  return isLoaded ? (
    <div className="map-container">
      <p>Your coordinates are: {[lat, ", ", long]}</p>
      <GoogleMap
        center={null}
        options={options}
        zoom={14}
        mapContainerStyle={{ width: "100%", height: "75vh" }}
        onLoad={initializeMap}
        onUnmount={onUnmount}
        onBoundsChanged={handleMapBoundsChange}
        onZoomChanged={() => {
          getLocations();
          handleMapBoundsChange();
        }}
        onDragEnd={() => {

          getLocations()

          handleMapBoundsChange();


        }}
        onClick={handleMapClick}
      >
        {markersWithinBounds.map((marker) => {
          if (marker.position.lat > southWest.lat()
            && marker.position.lat < northEast.lat()
            && marker.position.lng > southWest.lng()
            && marker.position.lng < northEast.lng()) {
            return (
              <MarkerF
                key={marker.id}
                position={marker.position}
                onClick={() => onMarkerClick(marker)}
                onCloseClick={() => {
                  setShowOtherInfoWindows(null);
                }}
              >
                {showOtherInfoWindows === marker.id && (
                  <InfoWindowF
                    position={marker.position}
                    onCloseClick={() => {
                      setShowOtherInfoWindows(null);
                    }}
                    map={map}
                  >
                    <div>
                      <h2>{marker.subject}</h2>
                      <div
                        style={{
                          backgroundImage: `url(${marker.picture_url})`,
                          backgroundSize: 'cover',
                          backgroundPosition: 'center',
                          width: '200px',
                          height: '100px',
                        }}
                      ></div>
                    </div>
                  </InfoWindowF>
                )}
              </MarkerF>
            );
          }
          return null;
        })}
        {tempMarkerPosition && (

          < MarkerF
            position={tempMarkerPosition}
            onClick={() => {
              setShowInfoWindow(true);
            }}
          >

            {showInfoWindow && tempMarkerPosition && (
              <InfoWindowF
                options={{
                  pixelOffset: new window.google.maps.Size(0, -5),
                }}
                position={tempMarkerPosition}
                onCloseClick={() => {
                  setShowInfoWindow(false);
                  setTempMarkerPosition(null);
                  setContentReady(false);
                }}
                map={map}
              >
                <div className="info-window-box">
                  <h2 className="experience_label">Create Experience?</h2>
                  <link
                    rel="stylesheet"
                    href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
                  />
                  <div className={`center ${contentReady ? "show" : ""}`} onClick={() => buttonClick(tempMarkerPosition.lat, tempMarkerPosition.lng)}>
                    <label className="label">
                      <input className="label__checkbox" type="checkbox" />
                      <span className="label__text">
                        <span className="label__check">
                          <i className="fa fa-check icon"></i>
                        </span>
                      </span>
                    </label>
                  </div>
                </div>
              </InfoWindowF>
            )}
          </MarkerF>
        )}
      </GoogleMap>
    </div >
  ) : null;
}

export default MyComponent;
