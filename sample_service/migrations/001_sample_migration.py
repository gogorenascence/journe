steps = [
    [
        """
        CREATE TABLE dummy (
            id SERIAL PRIMARY KEY NOT NULL,
            required_limited_text VARCHAR(1000) NOT NULL,
            required_unlimited_text TEXT NOT NULL,
            required_date_time TIMESTAMP NOT NULL,
            automatically_set_date_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
            required_integer INTEGER NOT NULL,
            required_money MONEY NOT NULL
        );
        """,
        """
        DROP TABLE dummy;
        """,
    ],
    [
        """
        CREATE TABLE big_dummy (
            id SERIAL PRIMARY KEY NOT NULL,
            required_limited_text VARCHAR(1000) NOT NULL,
            required_unlimited_text TEXT NOT NULL,
            required_date_time TIMESTAMP NOT NULL,
            automatically_set_date_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
            required_integer INTEGER NOT NULL,
            required_money MONEY NOT NULL
        );
        """,
        """
        DROP TABLE big_dummy;
        """,
    ],
    [
    """
    CREATE TABLE users (
        id SERIAL NOT NULL UNIQUE,
        first VARCHAR(50) NOT NULL,
        last VARCHAR(50) NOT NULL,
        email TEXT NOT NULL UNIQUE,
        password TEXT NOT NULL,
        username VARCHAR(50) NOT NULL UNIQUE,
        zodiac_sign TEXT
    );
    """,
    """
    DROP TABLE users;
    """,
    ],
    [
    """
    CREATE TABLE feeds (
        id serial not null unique,
        user_id int not null references users("id") on delete cascade,
        subject text,
        longitude float not null,
        latitude float not null,
        favorite boolean,
        post_date date not null,
        category text NOT NULL check(category = 'POI' or category = 'Event' or category = 'Other'),
        picture_url text not null,
        description text not null
    );
    """,
    """
    DROP TABLE feeds;
    """,
    ],
]
