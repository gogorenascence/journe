from fastapi import FastAPI, WebSocket, WebSocketDisconnect
from fastapi.middleware.cors import CORSMiddleware
from routers import feeds, users
from routers.authenticator import authenticator
import os

# from fastapi.responses import HTMLResponse
###

app = FastAPI()
app.include_router(feeds.router)
app.include_router(users.router)
app.include_router(authenticator.router)

origins = [
    "http://localhost:3000",
    os.environ.get("CORS_HOST", None),
    "https://jinp36.gitlab.io",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

connections = []


@app.get("/")
def home():
    return {"Test": "Hello, World!"}


@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    connections.append(websocket)

    try:
        while True:
            data = await websocket.receive_text()

            for connection in connections:
                if connection != websocket:
                    await connection.send_text(data)
    except WebSocketDisconnect:
        connections.remove(websocket)
