import os
import jwtdown_fastapi
from fastapi import Depends
from jwtdown_fastapi.authentication import Authenticator
from queries.users import UserQueries
from routers.models import UserOut


class JourneAuthenticator(Authenticator):
    async def get_account_data(
        self,
        username: str,
        users: UserQueries,
    ):
        return users.get_user_by_username(username)

    def get_account_getter(
        self,
        users: UserQueries = Depends(),
    ):
        return users

    def get_hashed_password(self, user: UserOut):
        return user["password"]

    def get_account_data_for_cookie(self, user: UserOut):
        return user["username"], UserOut(**user)


authenticator = JourneAuthenticator(os.environ["SIGNING_KEY"])
